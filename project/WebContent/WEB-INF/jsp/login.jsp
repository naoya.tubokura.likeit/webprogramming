<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>

<head>
     <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">

<html lang="ja">

<meta charset="UTF-8">

<title>ログイン画面</title>

</head>

<body>


	<c:if test="${er != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${er}
		</div>
	</c:if>

<body style="background-color: #D9E5FF;">


	 <span style="text-align: center">
		<p style="color: navy; background: #CFF;">
			<font size="7"> ログイン画面</font>
		</p>
	</span>




	<form action="Login" method="post">
		<span style="text-align: center">
				<input type="text" name="loginId" id="inputLoginId"
					class="form-control" placeholder="ログインID" required autofocus>
		</span>
		<span style="text-align: center">
				<input type="password" name="password" id="inputPassword"
					class="form-control" placeholder="パスワード" required>
		</span>
		<span style="text-align: center">
				<input type="submit" value="ログイン" class="btn btn-primary">
			</span>


	</form>
</body>
</html>