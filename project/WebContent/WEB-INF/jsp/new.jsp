<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<body style="background-color:#D9E5FF;">
<div style="position:absolute; top:1px; right:200px;">${userlnfo.name} さん</div>
<p><a href="Logout">ログアウト</a></p>
<span style="text-align: center"> <p style="color:navy; background:#CFF;"> <font size="7"> ユーザー新規登録</font></p></span>

<c:if test="${err!= null}" >
	    <div class="alert alert-danger" role="alert">
		  ${err}
		</div>
	</c:if>


<form action="Register" method="post">
<span style="text-align: center"><p>
ログインID：<input type="text" name="login_id" size="40"></span>
</p>

<span style="text-align: center"><p>
パスワード：<input type="text" name="password1" size="40"></span>
</p>


<span style="text-align: center"><p>
パスワード(確認)：<input type="text" name="password2" size="40"></span>
</p>


<span style="text-align: center"><p>
ユーザー名：<input type="text" name="name" size="40"></span>
</p>


<span style="text-align: center"><p>
生年月日<input type="text" name="birth_date" size="40"></span>
</p>

<p class="textright"><input type="submit" id="refine_search" value="登録"></p>
</span>
</form>
<span style="text-align: right">
<p><a href="userList">戻る</a></p>

</span>

</body>
</html>