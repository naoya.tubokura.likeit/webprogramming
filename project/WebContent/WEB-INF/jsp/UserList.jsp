<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title></title>
</head>

<body>
<body style="background-color: #D9E5FF;">
	<div style="position: absolute; top: 1px; right: 200px;">${userlnfo.name}
		さんログイン中</div>

	<p>
		<a href="Logout">ログアウト</a>
	</p>
	<span style="text-align: center">
		<p style="color: navy; background: #CFF;">
			<font size="7"> ユーザー一覧</font>
		</p>
		</sp>
		<p>
			<a href="Register">新規登録</a>
		</p>
		<div class="panel-body">


				<c:if test="${err != null}">
					<div class="alert alert-danger" role="alert">${er}</div>
				</c:if>
				<form action="userList" method="post">
					<span style="text-align: center"><p>

							ログインID：<input type="text" name="login_id" size="40"></span>
					</p>

					<span style="text-align: center"><p>
							ユーザー名：<input type="text" name="name" size="40"></span>
					</p>

					<span style="text-align: center"><p>
							<input type="date" id="date_s"  name="startDate"  maxlength="10">～<input
								type="date" id="date_f"  name="endDate"  maxlength="10"></span>
					</p>

					<p class="textright">
						<input type="submit" id="refine_search" value="検索">
					</p>
				</form>
				<div class="table-responsive">
					<table class="table table-striped">
						<thead>
							<tr>
								<th>ログインID</th>
								<th>ユーザ名</th>
								<th>生年月日</th>
								<th>機能</th>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						</thead>
						<tbody>
						
							<c:forEach var="user" items="${userList}">
								<tr>
									<td>${user.login_id}</td>
									<td>${user.name}</td>
									<td>${user.birth_date}</td>

									<td><a class="btn btn-primary" href="refer?id=${user.id}">詳細</a>

										<c:if test="${userlnfo.login_id == 'admin'}">
											<a class="btn btn-success" href="renewal?id=${user.id}">更新</a>
										</c:if> <c:if test="${userlnfo.login_id != 'admin'}">
											<c:if test="${user.login_id == userlnfo.login_id}">
												<a class="btn btn-success" href="renewal?id=${user.id}">更新</a>
											</c:if>

										</c:if> <c:if test="${userlnfo.login_id == 'admin'}">
											<a class="btn btn-danger" href="delete?id=${user.id}">削除</a>
										</c:if></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>








				</td>
				</tr>





				</tbody>
				</tabale>
				</table>




</body>
</html>