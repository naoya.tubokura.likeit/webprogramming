package DAO;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.xml.bind.DatatypeConverter;

/**
 * Servlet implementation class userDAO
 */
@WebServlet("/userDAO")
public class userDAO {




	public UserBeans findByLoginlofo(String login_Id, String password) {

		Connection conn = null;
		try {
			conn = DataBase.getConnection();
			String sql = "select * from user where login_id = ? and password = ?";


			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, login_Id);
			pStmt.setString(2,seacret(password));



			ResultSet rs = pStmt.executeQuery();


			if (!rs.next()) {
				return null;



			}
			String loginidData = rs.getString("login_Id");
			String nameData = rs.getString("name");


			return new UserBeans(loginidData, nameData);

		} catch (SQLException e) {

			e.printStackTrace();
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}

		return null;

	}

	public List<UserBeans> findAll() {
		Connection conn = null;
		List<UserBeans> userList = new ArrayList<UserBeans>();

		try {
			conn = DataBase.getConnection();
			String sql = "select * from user where login_id not in ('admin') ";

			java.sql.Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery(sql);

			while (rs.next()) {
				int id1 = rs.getInt("id");
				String login_id1 = rs.getString("login_id");
				String name1 = rs.getString("name");
				Date birth_date1 = rs.getDate("birth_date");
				String password1 = rs.getString("password");
				String create_date1 = rs.getString("create_date");
				String update_date1 = rs.getString("update_date");
				UserBeans user = new UserBeans(id1, login_id1, name1, birth_date1, password1, create_date1, update_date1);

				userList.add(user);

			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return userList;
	}

	public void Insert(String login, String name, String birth_date, String password1) {
		Connection conn = null;
		try {
			conn = DataBase.getConnection();
			String sql = "insert into user (login_id,name,birth_date, password,create_date,update_date) values (?,?,?,?,NOW(),NOW()) ";


			PreparedStatement pr = conn.prepareStatement(sql);
			pr.setString(1, login);
			pr.setString(2, name);
			pr.setString(3, birth_date);
			pr.setString(4, seacret(password1));

			int rs = pr.executeUpdate();

			System.out.println(rs);
			pr.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}


	}

	public UserBeans confirm() {
		Connection conn = null;
		try {
			conn = DataBase.getConnection();
			String sql = "select login_id from user where login_id=? ";
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery(sql);

			if (rs.next()) {
				return null;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
		return null;

	}

	public UserBeans finddouble(String login_Id) {
		Connection conn = null;
		try {
			conn = DataBase.getConnection();
			String sql = "select * from user where login_id = ? ";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, login_Id);
			ResultSet rs = pStmt.executeQuery();
			if (!rs.next()) {
				return null;

			}
			String loginidData = rs.getString("login_Id");
			return new UserBeans(loginidData);


		} catch (SQLException e) {

			e.printStackTrace();
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}

		return null;

	}




	public void delete (String login_id) {
		Connection conn = null;

		try {
			conn = DataBase.getConnection();
			String sql = "delete from user where login_id = ? ";
			PreparedStatement pr = conn.prepareStatement(sql);
			pr.setString(1, login_id);

			int rs= pr.executeUpdate();

			System.out.println(rs);
			pr.close();


		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}


	}


	public UserBeans refer(String id) {
		Connection conn = null;
		try {
			conn = DataBase.getConnection();
			String sql = "select * from user where id = ? ";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1,id);

			ResultSet rs = pStmt.executeQuery();
			if (!rs.next()) {
				return null;
			}


			String loginidData = rs.getString("login_Id");
			String nameData = rs.getString("name");
			Date Birthdate = rs.getDate("birth_date");
			String create_date = rs.getString("create_date");
			String update_date = rs.getString("update_date");
			int id1 = rs.getInt("id");

			return new UserBeans( id1,loginidData,nameData,Birthdate,create_date,update_date);

		} catch (SQLException e) {

			e.printStackTrace();
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}

		return null;

	}


	public  void update(String name, String birth_date, String id) {
		Connection conn = null;
		try {
			conn = DataBase.getConnection();
			String sql = "update user set name= ? ,birth_date= ? where id =?";
			PreparedStatement st = conn.prepareStatement(sql);




			st.setString(1, name);
			st.setString(2, birth_date);
			st.setString(3, id);
			int rs = st.executeUpdate();



			System.out.println(rs);
			st.close();




		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}



	}
	public  void update(String name, String birth_date, String password1,String id) {
		Connection conn = null;
		try {
			conn = DataBase.getConnection();
			String sql = "update user set  name = ? , birth_date= ?, password=? where id =?";
			PreparedStatement st = conn.prepareStatement(sql);

			seacret( password1);
			st.setString(1, name);
			st.setString(2, birth_date);
			st.setString(3, seacret(password1));

			st.setString(4, id);
			int rs = st.executeUpdate();



			System.out.println(rs);
			st.close();




		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}



	}


	public String seacret(String password) {

		String source = password;


		Charset charset = StandardCharsets.UTF_8;

		String algorithm = "MD5";


		byte[] bytes = null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e) {

			e.printStackTrace();
		}
		String result = DatatypeConverter.printHexBinary(bytes);


		return result;
	}



	public List<UserBeans> findSearch(String name,String login_id,String startDate,String endDate) {
		Connection conn = null;
		List<UserBeans> userList = new ArrayList<UserBeans>();

		try {
			conn = DataBase.getConnection();
			String sql = "select * from user where login_id not in ('admin') ";

			if(!login_id.equals("")) {
				sql += " and login_id = '" + login_id + "'";
			}
			if(!name.equals("")) {
				sql +="and  name LIKE '%" + name + "%'";
			}
			if(!startDate.equals("")) {
				sql +="and birth_date>='"+startDate+"'";
			}
			if(!endDate.equals("")) {
				sql += "and birth_date<='"+endDate+"'";
			}

			System.out.println(sql);
			java.sql.Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery(sql);

			while (rs.next()) {
				int id1 = rs.getInt("id");
				String login_id1 = rs.getString("login_id");
				String name1 = rs.getString("name");
				Date birth_date1 = rs.getDate("birth_date");
				String password1 = rs.getString("password");
				String create_date1 = rs.getString("create_date");
				String update_date1 = rs.getString("update_date");
				UserBeans user = new UserBeans(id1, login_id1, name1, birth_date1, password1, create_date1, update_date1);

				userList.add(user);

			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return userList;
	}







}
