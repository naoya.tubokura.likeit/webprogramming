package function;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DAO.UserBeans;
import DAO.userDAO;


/**
 * Servlet implementation class userList
 */
@WebServlet("/userList")
public class userList extends HttpServlet {
	private static final long serialVersionUID = 1L;


    public userList() {
        super();

    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		 request.setCharacterEncoding("UTF-8");

		 userDAO userDao = new userDAO();


		List<UserBeans> userList = userDao.findAll();
			request.setAttribute("userList", userList);


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserList.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		String name = request.getParameter("name");
		String login_id = request.getParameter("login_id");
		String birth_date = request.getParameter("startDate");
		String birth_date1 = request.getParameter("endDate");

		userDAO userDAO = new userDAO();
		List<UserBeans> userList = userDAO.findSearch(name, login_id, birth_date, birth_date1);

		request.setAttribute("userList", userList);


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserList.jsp");
		dispatcher.forward(request, response);









	}

}
