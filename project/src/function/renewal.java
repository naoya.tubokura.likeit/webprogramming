package function;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DAO.UserBeans;
import DAO.userDAO;

/**
 * Servlet implementation class renewal
 */
@WebServlet("/renewal")
public class renewal extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public renewal() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String id = request.getParameter("id");

		userDAO userdao = new userDAO();
		UserBeans user1 = userdao.refer(id);

		request.setAttribute("refer", user1);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/renewal.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String password1 = request.getParameter("password1");
		String password2 = request.getParameter("password2");

		String name = request.getParameter("name");
		String birth_date = request.getParameter("birth_date");

		String id = request.getParameter("id");

		userDAO userdao = new userDAO();
		UserBeans user1 = userdao.refer(id);

		request.setAttribute("refer", user1);


		if(password1.equals("")&&password2==("")){
			userDAO depass = new userDAO();
			depass.update(name,birth_date,id);
		}else{
			userDAO u = new userDAO();
			u.update(name, birth_date, password1,id);

		if (password2.equals("") || name == ("") || birth_date == ("") || password1 == ("")) {
			request.setAttribute("errr", "入力された内容は正しくありません");
			RequestDispatcher dispatcher1 = request.getRequestDispatcher("/WEB-INF/jsp/renewal.jsp");
			dispatcher1.forward(request, response);
		}
		if(!password1.equals(password2)) {
			request.setAttribute("errr", "入力された内容は正しくありません");
			RequestDispatcher dispatcher1 = request.getRequestDispatcher("/WEB-INF/jsp/renewal.jsp");
			dispatcher1.forward(request, response);
		}
		}



			response.sendRedirect("userList");


	}

}
