package function;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DAO.UserBeans;
import DAO.userDAO;

/**
 * Servlet implementation class Register
 */
@WebServlet("/Register")
public class Register extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Register() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/new.jsp");
		dispatcher.forward(request, response);



	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		String login_id = request.getParameter("login_id");
		String name = request.getParameter("name");
		String birth_date = request.getParameter("birth_date");

		String password1 = request.getParameter("password1");
		String password2 = request.getParameter("password2");

		userDAO userdao = new userDAO();
		UserBeans user1 = userdao.finddouble(login_id);

		if (user1 != null) {
			request.setAttribute("err", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/new.jsp");
			dispatcher.forward(request, response);

			return;

		}

		if (password2.equals("") || login_id == ("") || name == ("") || birth_date == ("") || password1 == ("")) {
			request.setAttribute("err", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/new.jsp");
			dispatcher.forward(request, response);

			return;

		}

		if (!password1.equals(password2)) {

			request.setAttribute("err", "入力された内容は正しくありません");
			RequestDispatcher dispatcher1 = request.getRequestDispatcher("/WEB-INF/jsp/new.jsp");
			dispatcher1.forward(request, response);

			return;

		}

		userDAO u = new userDAO();
		u.Insert(login_id, name, birth_date, password1);

		response.sendRedirect("userList");

	}

}
