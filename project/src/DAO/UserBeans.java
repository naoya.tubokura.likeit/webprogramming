package DAO;

import java.io.Serializable;
import java.sql.Date;

public class UserBeans implements Serializable {
	private int id;
	private String login_id;
	private String name;
	private	 Date birth_date;
	private String password;
	private String create_date;
	private String update_date;

	public UserBeans(String name) {

	}


	public UserBeans(String name, Date birth_date, String password,int id) {
		this.name = name;
		this.birth_date = birth_date;
		this.password = password;
		this.id = id;
	}


	public UserBeans(String name ,Date birth_date) {
		this.name = name;
		this.birth_date=birth_date;
	}




	public UserBeans(String loginid, String name) {
		this.login_id = loginid;
		this.name = name;
	}










	public UserBeans(String loginidData, String nameData, Date birthdate, String create_date2, String update_date2,
			int id1) {
		this.login_id=loginidData;
		this.name=nameData;
		this.birth_date=birthdate;
		this.create_date=create_date2;
		this.update_date=update_date2;
	}



	public UserBeans(int id1 ,String login_id1, String name1, Date birth_date1, String password1, String create_date1,
			String update_date1) {
		this.id= id1;
		this.login_id=login_id1;
		this.name=name1;
		this.birth_date=birth_date1;
		this.password=password1;
		this.create_date=create_date1;
	}







	public UserBeans(String loginidData, String nameData, Date birth_date1) {
		this.login_id=loginidData;
		this.name=nameData;
		this.birth_date=birth_date1;
	}







	public UserBeans(int id1, String loginidData, String nameData, Date birthdate, String create_date2,
			String update_date2) {
		this.id=id1;
		this.login_id=loginidData;
		this.name=nameData;
		this.birth_date=birthdate;
		this.create_date=create_date2;
	}






	public UserBeans(int id1, String login_id1, String name1, Date birth_date1, String password1, String create_date1,
			String update_date1, Date birth_date2, Date birth_date3) {
		this.id=id1;
		this.login_id=login_id1;
		this.name=name1;
		this.birth_date=birth_date1;
		this.password=password1;
		this.create_date=create_date1;
		this.update_date=update_date1;
		this.birth_date=birth_date2;
		this.birth_date=birth_date3;
	}







	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getLogin_id() {
		return login_id;
	}


	public void setLogin_id(String login_id) {
		this.login_id = login_id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Date getBirth_date() {
		return birth_date;
	}


	public void setBirth_date(Date birth_date) {
		this.birth_date = birth_date;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getCreate_date() {
		return create_date;
	}


	public void setCreate_date(String create_date) {
		this.create_date = create_date;
	}


	public String getUpdate_date() {
		return update_date;
	}


	public void setUpdate_date(String update_date) {
		this.update_date = update_date;
	}


}
